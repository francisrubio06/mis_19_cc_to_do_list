<?php
    include("db_conexion.php");
    $sql_query = "SELECT * FROM tareas";

    echo "<script>console.log('Running Query')</script>";
    echo "<script>console.log('" . $sql_query . "')</script>";

    $result = $conexion->query($sql_query);

    // Verificar si fallo la consulta
    if(!$result){
        echo "Error al ejecutar el query: \n\t" . $sql_query . "\n";
        echo "Errno: " . $mysqli->errno . "\n";
        echo "Error: " . $mysqli->error . "\n";
    }

    $conexion->close();
?>