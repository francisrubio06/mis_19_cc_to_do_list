<?php

    // Cambiar el valor dependiendo de donde se va a ejecutar
    // 0 = La aplicación corre en computador local (Desarrollo)
    // 1 = La aplicación corre en producción (Produccion)
    $run_mode = 1;

    if($run_mode == 0){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dataBase = "cp_to_do_list";
    }elseif ($run_mode == 1) {
        $servername = "mysql";
        $username = "root";
        $password = ".root";
        $dataBase = "cp_to_do_list";
    }

    // Create connection
    $conexion = new mysqli($servername, $username, $password, $dataBase);

    // Check connection
    if ($conexion->connect_error) {
        die("Connection failed: " . $conexion->connect_error);
    }
    echo "<script>console.log('Connected successfully')</script>";
?>